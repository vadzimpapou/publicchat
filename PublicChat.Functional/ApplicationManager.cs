﻿using System;
using System.Collections.Generic;
using NHibernate;
using PublicChat.Core;
using PublicChat.DataAccess;
using PublicChat.Functional.Service;


namespace PublicChat.Functional
{
    public static class ApplicationManager
    {
        public static Version ApplicationVersion = new Version(0, 0, 1, 0);

        public static string SystemVersionKey = ApplicationVersion.ToString().Replace(".", "_");

        public static ISession NHibernateSession
        {
            get { return NHibernateSessionManager.Instance.GetSession(); }
        }

        public static void OpenDataTransaction()
        {
            NHibernateSessionManager.Instance.BeginTransaction();
        }

        public static void CloseDataTransaction()
        {
            try
            {
                NHibernateSessionManager.Instance.CommitTransaction();
            }
            finally
            {
                NHibernateSessionManager.Instance.CloseSession();
            }
        }

        public static void UpdateDatabaseSchema()
        {
            NHibernateSessionManager.Instance.UpdateSchema();
//            var message = new Message {AuthorName = "DEVELOPER", Text = "Welcome"};
           // new MessageService().SaveMessage(message);
        }

    }
}