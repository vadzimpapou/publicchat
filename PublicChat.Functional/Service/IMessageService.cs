﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PublicChat.Core;

namespace PublicChat.Functional.Service
{
    interface IMessageService
    {
        Message GetMessageById(int id);

        Message GetFirstMessage();

        Message GetLastMessage();
        
        IEnumerable<Message> GetAllMessages(); 

        void SaveMessage(Message message);

    }
}
