﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PublicChat.Core;

namespace PublicChat
{
    public class ApplicationContext
    {
        private const string USERS_ONLINE = "UsersOnline";  
        private const string BLOCK_LIST = "BlockList";       
        private const string REFRESH_LIST = "Refresh";     

        public ApplicationContext()
        {
            HttpContext.Current.Application[USERS_ONLINE] = new Dictionary<string,string>();// список активных юзеров 
            HttpContext.Current.Application[REFRESH_LIST] = new List<string>(); // список юзеров для которых необходимо обновить страницу
            HttpContext.Current.Application[BLOCK_LIST] = new Dictionary<string, DateTime>();// список заблокированных/удаленных юзеров
        }

        /// <summary>
        /// Получение списка имен активных пользователей
        /// </summary>
        /// <returns></returns>
        public List<string> GetOnlineUserNames()
        {
            return ((Dictionary<string, string>)HttpContext.Current.Application[USERS_ONLINE]).Values.ToList();
        }

        /// <summary>
        /// Получение имени активного пользователя по идентификатору сессии
        /// </summary>
        /// <param name="sessionId"></param>
        /// <returns></returns>
        public string GetUserNameBySessionId(string sessionId)
        {
            return ((Dictionary<string, string>)HttpContext.Current.Application[USERS_ONLINE]).FirstOrDefault(m => m.Key == sessionId).Value;

        }

        /// <summary>
        /// Получение коллекции пар значений ID сессии - имя пользователя
        /// </summary>
        /// <returns></returns>
        public Dictionary<string,string> GetUserSessions()
        {
            return (Dictionary<string, string>) HttpContext.Current.Application[USERS_ONLINE];
        }

        /// <summary>
        /// Проверка находится ли пользователь с данным именем в чате
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public bool IsUserOnlineByName(string userName)
        {
            return ((Dictionary<string, string>) HttpContext.Current.Application[USERS_ONLINE]).ContainsValue(userName);
        }

        /// <summary>
        /// Проверка находится ли пользователь с данным идентификатором сессии в чате
        /// </summary>
        /// <param name="sessionId"></param>
        /// <returns></returns>
        public bool IsUserOnlineBySessionId(string sessionId)
        {
            return ((Dictionary<string, string>)HttpContext.Current.Application[USERS_ONLINE]).ContainsKey(sessionId);
        }

        /// <summary>
        /// Добавление в коллекцию активных пользователей новой пары значений ID сессии - имя пользоватея
        /// </summary>
        /// <param name="sessionId"></param>
        /// <param name="userName"></param>
        public void AddOnlineUser(string sessionId, string userName)
        {
            ((Dictionary<string, string>)HttpContext.Current.Application[USERS_ONLINE]).Add(sessionId,userName);
        }

        /// <summary>
        /// Удаление из коллекции активных пользователей записи по ID сессии
        /// </summary>
        /// <param name="sessionId"></param>
        public void RemoveOnlineUser(string sessionId)
        {
            ((Dictionary<string, string>)HttpContext.Current.Application[USERS_ONLINE]).Remove(sessionId);
        }

        /// <summary>
        /// Получение списка заблокированных и удаленных пользователей
        /// </summary>
        /// <returns></returns>
        public List<string> GetBlockList()
        {
            return
                ((Dictionary<string, DateTime>) HttpContext.Current.Application[BLOCK_LIST]).Select(m => m.Key).ToList();
        }

        /// <summary>
        /// Получение даты окончания блокировки или null если юзера нет в блок-листе
        /// </summary>
        /// <param name="userName"> </param>
        /// <returns></returns>
        public DateTime? GetBlockExpiresDate(string userName)
        {
            if (((Dictionary<string, DateTime>)HttpContext.Current.Application[BLOCK_LIST]).ContainsKey(userName))
                return ((Dictionary<string, DateTime>)HttpContext.Current.Application[BLOCK_LIST])[userName];
             return null;
        }

        /// <summary>
        /// Установка даты, до которой включается блокировка, для пользователя с таким именем
        /// </summary>
        /// <param name="userName"> </param>
        /// <param name="blockUntil"></param>
        public void BlockUserUntil(string userName, DateTime blockUntil)
        {
            if (((Dictionary<string, DateTime>)HttpContext.Current.Application[BLOCK_LIST]).ContainsKey(userName))
            {
                ((Dictionary<string, DateTime>)HttpContext.Current.Application[BLOCK_LIST])[userName] = blockUntil;
            }
            else
            {
                ((Dictionary<string, DateTime>)HttpContext.Current.Application[BLOCK_LIST]).Add(userName, blockUntil);
            }
        }

        /// <summary>
        /// Удаление пользователя с таким именем из списка заблокированных
        /// </summary>
        /// <param name="userName"> </param>
        public void RemoveUserFromBlockList(string userName)
        {
            ((Dictionary<string, DateTime>) HttpContext.Current.Application[BLOCK_LIST]).Remove(userName);
        }

        /// <summary>
        /// Возвращает список пользователей, для которых необходимо перегрузить страницу
        /// </summary>
        /// <returns></returns>
        public List<string> GetUsersToRefresh()
        {
            return (List<string>)HttpContext.Current.Application[REFRESH_LIST];
        }

        /// <summary>
        /// Установка списка пользователей для которых необходимо перегрузить страницу (копируются имена пользователей из коллекции активных пользователей)
        /// </summary>
        public void SetUsersToRefresh()
        {
            var onlineUserNames =
                ((Dictionary<string, string>) HttpContext.Current.Application[USERS_ONLINE]).Values.ToList();
            for (int i = 0; i < onlineUserNames.Count; i++)
            {
                ((List<string>)HttpContext.Current.Application[REFRESH_LIST]).Add(onlineUserNames[i]);
            }
        }
        /// <summary>
        /// Удаление пользователя из списка для о
        /// </summary>
        /// <param name="userName"></param>
        public void RemoveUserFromRefreshList(string userName)
        {
            ((List<string>)HttpContext.Current.Application[REFRESH_LIST]).Remove(userName);
        }
        //-----------------------------------------------------------------------------------------------
        
        public void CreateCookie(string userName)
        {
            HttpContext.Current.Response.Cookies.Add(new HttpCookie(userName));
        }

        public bool CookieExists(string userName)
        {
            return (HttpContext.Current.Response.Cookies.AllKeys.Contains(userName));
          //  return (HttpContext.Current.Response.Cookies.Get(userName) != null);
        }

    }
}