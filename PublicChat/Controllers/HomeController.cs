﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using PublicChat.Core;
using PublicChat.Functional;
using PublicChat.Functional.Service;

namespace PublicChat.Controllers
{
    public class HomeController : Controller
    {
        private const string FOREVER_BAN = "01/01/2100";
        private const string WELCOME = "Добро пожаловать в наш чат!";
        private const string USER_EXISTS = "Пользователь с таким именем существует";
        private static string _adminName = "";
        private static ApplicationContext _applicationContext = new ApplicationContext();
        private readonly MessageService _messageService = new MessageService();

        public ActionResult Update()
        {
            ApplicationManager.UpdateDatabaseSchema();
            return RedirectToAction("Index");
        }

        public ActionResult Index()
        {
            if (_applicationContext.IsUserOnlineBySessionId(Session.SessionID)) 
            {
                var user = new User { Name = _applicationContext.GetUserNameBySessionId(Session.SessionID) };
                return RedirectToAction("Chat", user);
            }

            ViewBag.Message = WELCOME;
            return View();
        }

        [HttpGet]
        public ActionResult Chat(User user)
        {
            if (!_applicationContext.IsUserOnlineBySessionId(Session.SessionID))
            {
             //   if (!_applicationContext.GetOnlineUserNames().Contains(user.Name))
                    // if (!_applicationContext.CookieExists(user.Name))
             //   {
                    _applicationContext.AddOnlineUser(Session.SessionID, user.Name);
                    //       _applicationContext.CreateCookie(user.Name);//todo вынести куки в отдельный класс
                }
                /*else
                {
                    ViewBag.NameExists = USER_EXISTS;
                    return View("Index");
                }*/
           // }

            ViewBag.UserName = user.Name;

            #region User is Admin ?


            
            if (String.IsNullOrEmpty(_adminName))
            {
               // string firstOnlineUserName = _applicationContext.GetOnlineUserNames().FirstOrDefault();
                //ViewBag.IsAdmin = String.Equals(firstOnlineUserName, user.Name);
                //if (ViewBag.IsAdmin)
                {
                    ViewBag.IsAdmin = true;
                    _adminName = user.Name;
                }
            }
            else 
            {
                ViewBag.IsAdmin = String.Equals(_adminName, user.Name);
            }

            #endregion

            var firstMessage = _messageService.GetFirstMessage();
            return View("Chat", firstMessage);
        }



        /// <summary>
        /// Отправка сообщения в БД
        /// </summary>
        /// <param name="id"> </param>
        /// <param name="message"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SendMessage(int id, string message)
        {
            if (id == 0)
            {
                _messageService.SaveMessage(new Message {Text = message, AuthorName = _applicationContext.GetUserNameBySessionId(Session.SessionID)});
            }
            else
            {   
                _applicationContext.SetUsersToRefresh();
                var tempmessage = _messageService.GetMessageById(id);
                tempmessage.Text = message;
                _messageService.SaveMessage(tempmessage);
            }

            return Json(null);
        }

        /// <summary>
        /// Блокирует для пользователя возможность отправлять сообщения
        /// </summary>
        /// <param name="userName">Имя пользователя</param>
        /// <param name="blockTime">Время блокировки в минутах</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult BlockUser(string userName, string blockTime)
        {
            int blockTimeInt;
            bool parseOk = int.TryParse(blockTime, out blockTimeInt);
            bool userExists = _applicationContext.IsUserOnlineByName( userName );
            bool isAdmin = String.Equals(userName, _adminName);
            if (userExists && !isAdmin)
            {
                _applicationContext.BlockUserUntil(userName,
                                                   parseOk
                                                       ? DateTime.Now.AddMinutes(blockTimeInt)
                                                       : DateTime.Parse(FOREVER_BAN));          
            }
            return Json(new {userExists,isAdmin});
        }

        [HttpPost]
        public ActionResult IsBlocked(string userName)
        {
            bool isBlocked;
            bool isBlockedForever;
            var blockExpiresDate = _applicationContext.GetBlockExpiresDate(userName);
            string unblockTime = "";
            if (_applicationContext.GetBlockList().Contains(userName) && blockExpiresDate != null)
           {
               isBlocked = blockExpiresDate > DateTime.Now;
               isBlockedForever = blockExpiresDate == DateTime.Parse(FOREVER_BAN);
               unblockTime = ((DateTime) blockExpiresDate).ToShortTimeString();
           }
            else
            {
                isBlocked = false;
                isBlockedForever = false;
                _applicationContext.RemoveUserFromBlockList(userName);
            }

            return
                Json(
                    new
                        {
                            isBlocked,
                            isBlockedForever,
                            unblockTime
                        });
        }

        [HttpPost]
        public ActionResult GetAllMessages()
        {
            var messages = _messageService.GetAllMessages().ToArray();
            return
                   Json(
                       new
                       {
                           messageList = messages,
                       });
        }

        /// <summary>
        /// Метод возвращает последнее сообщение в чат
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetLatestMessage(int? id, string userName)
        {
            bool refresh = false; // если true - необходимо обновить страницу на клиенте

            if (id == null) id = 0;

            if (_applicationContext.IsUserOnlineBySessionId(Session.SessionID))
            {
                if (_applicationContext.GetUsersToRefresh().Contains(_applicationContext.GetUserNameBySessionId(Session.SessionID)))
                {
                    refresh = true;
                    _applicationContext.RemoveUserFromRefreshList(_applicationContext.GetUserNameBySessionId(Session.SessionID));
                }
            }

            var lastMessage = _messageService.GetLastMessage();
            if (lastMessage != null && id < lastMessage.Id)
            {
                var message = _messageService.GetMessageById((int) id + 1);
                return
                    Json(
                        new
                            {
                                authorName = message.AuthorName, 
                                text = message.Text,
                                idLatest = message.Id,  
                                needreload = refresh
                            });
            }
            return Json(new { needreload = refresh });
        }
        /// <summary>
        /// Получение сообщения для редактирования
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetMessageToEdit(int? id)
        {
            if (id != null)
            {
                var messageToEdit = _messageService.GetMessageById((int) id);
                if (messageToEdit != null)
                {
                    return
                        Json(
                            new
                                {
                                    text = messageToEdit.Text,
                                });
                }
            }
            return Json(null);
        }

        [HttpPost]
        public ActionResult GetUsersOnlineList()
        {
            var activeSessions = MvcApplication.Sessions;
            var usersOnline = _applicationContext.GetUserSessions();
            var keysToDelete = new List<string>();
            foreach ( KeyValuePair<string,string> keyValuePair in usersOnline)
            {
                if ( !activeSessions.Contains(keyValuePair.Key))
                    keysToDelete.Add(keyValuePair.Key);
            }

            foreach (string key in keysToDelete)
            {
                _applicationContext.RemoveOnlineUser(key);
            }
            
            
            return Json(new {toUsersOnline = _applicationContext.GetOnlineUserNames().ToArray() });
        }

        [HttpPost]
        public ActionResult OnExit()
        {
           
            return Json(null);
        }

        public JsonResult IsUserRegistered(string name)
        {
            if (!_applicationContext.IsUserOnlineByName(name))
                return Json(true, JsonRequestBehavior.AllowGet);
            return Json("Имя занято",JsonRequestBehavior.AllowGet);
        }
    }
}
