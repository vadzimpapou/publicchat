﻿using System.Runtime.Remoting.Messaging;
using System.Web;
using FluentNHibernate.Cfg;
using NHibernate;
using NHibernate.ByteCode.Castle;
using NHibernate.Cache;
using NHibernate.Cfg;
using NHibernate.Cfg.Loquacious;
using NHibernate.Dialect;
using NHibernate.Driver;
using NHibernate.Exceptions;
using NHibernate.Tool.hbm2ddl;
using PublicChat.Core.Mappings;


namespace PublicChat.DataAccess
{
    public sealed class NHibernateSessionManager
    {
        #region Thread-safe, lazy Singleton

        public static NHibernateSessionManager Instance
        {
            get
            {
                return Nested.NHibernateSessionManager;
            }
        }

        /// <summary>
        /// Initializes the NHibernate session factory upon instantiation.
        /// </summary>
        private NHibernateSessionManager()
        {
            InitSessionFactory();
        }

        /// <summary>
        /// Assists with ensuring thread-safe, lazy singleton
        /// </summary>
        private class Nested
        {
            static Nested() { }
            internal static readonly NHibernateSessionManager NHibernateSessionManager =
                new NHibernateSessionManager();
        }

        #endregion

        public void UpdateSchema()
        {
            new SchemaExport(_configuration).Execute(false, true, false);
        }

        private void InitSessionFactory()
        {
            string conStr = @"Data Source=PublicChat.mssql.somee.com;Initial Catalog=PublicChat;persist security info=False;user id=amikulich_SQLLogin_1;pwd=qgur2rjasu;";
                //@"Data Source=.\SQLEXPRESS;AttachDbFilename='D:\Temp\rg2\rg\trunk\source\RedGrand\App_Data\RG_db.mdf';Integrated Security=True;User Instance=True";
//            string conStr = "Data Source=(local);Initial Catalog=RG;Integrated Security=True";
            _configuration = new Configuration();
            _configuration.SessionFactory()
                    .Proxy.Through<ProxyFactoryFactory>()
                    .Integrate
                        .Using<MsSql2008Dialect>()
                        .AutoQuoteKeywords()
                        .Connected
                            .By<SqlClientDriver>()
                            .Using(conStr)
//                            .ByAppConfing("ApplicationServices")
                            .CreateCommands
                            .ConvertingExceptionsThrough<SQLStateConverter>();

            _sessionFactory =
                Fluently.Configure(_configuration).Mappings(m => m.FluentMappings.AddFromAssemblyOf<MessageMapping>()).
                    BuildSessionFactory();

//            _sessionFactory = Fluently.Configure()
//                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<PersistenceObject>())
//                .BuildSessionFactory();

//            _sessionFactory = new Configuration().Configure().BuildSessionFactory();
        }

        /// <summary>
        /// Allows you to register an interceptor on a new session.  This may not be called if there is already
        /// an open session attached to the HttpContext.  If you have an interceptor to be used, modify
        /// the HttpModule to call this before calling BeginTransaction().
        /// </summary>
        public void RegisterInterceptor(IInterceptor interceptor)
        {
            ISession session = ContextSession;

            if (session != null && session.IsOpen)
            {
                throw new CacheException("You cannot register an interceptor once a session has already been opened");
            }

            GetSession(interceptor);
        }

        public ISession GetSession()
        {
            return GetSession(null);
        }

        /// <summary>
        /// Gets a session with or without an interceptor.  This method is not called directly; instead,
        /// it gets invoked from other public methods.
        /// </summary>
        private ISession GetSession(IInterceptor interceptor)
        {
            ISession session = ContextSession;

            if (session == null)
            {
                if (interceptor != null)
                {
                    session = _sessionFactory.OpenSession(interceptor);
                }
                else
                {
                    session = _sessionFactory.OpenSession();
                }

                ContextSession = session;
            }
            
            return session;
        }

        /// <summary>
        /// Flushes anything left in the session and closes the connection.
        /// </summary>
        public void CloseSession()
        {
            ISession session = ContextSession;

            if (session != null && session.IsOpen)
            {
                session.Flush();
                session.Close();
            }

            ContextSession = null;
        }

        public void BeginTransaction()
        {
            ITransaction transaction = ContextTransaction;

            if (transaction == null)
            {
                transaction = GetSession().BeginTransaction();
                ContextTransaction = transaction;
            }
        }

        public void CommitTransaction()
        {
            ITransaction transaction = ContextTransaction;

            try
            {
                if (HasOpenTransaction())
                {
                    transaction.Commit();
                    ContextTransaction = null;
                }
            }
            catch (HibernateException)
            {
                RollbackTransaction();
                throw;
            }
        }

        public bool HasOpenTransaction()
        {
            ITransaction transaction = ContextTransaction;

            return transaction != null && !transaction.WasCommitted && !transaction.WasRolledBack;
        }

        public void RollbackTransaction()
        {
            ITransaction transaction = ContextTransaction;

            try
            {
                if (HasOpenTransaction())
                {
                    transaction.Rollback();
                }

                ContextTransaction = null;
            }
            finally
            {
                CloseSession();
            }
        }

        private static ITransaction ContextTransaction
        {
            get
            {
                if (IsInWebContext())
                {
                    return (ITransaction)HttpContext.Current.Items[TransactionKey];
                }
                return (ITransaction)CallContext.GetData(TransactionKey);
            }
            set
            {
                if (IsInWebContext())
                {
                    HttpContext.Current.Items[TransactionKey] = value;
                }
                else
                {
                    CallContext.SetData(TransactionKey, value);
                }
            }
        }

        private static ISession ContextSession
        {
            get
            {
                if (IsInWebContext())
                {
                    return (ISession)HttpContext.Current.Items[SessionKey];
                }
                return (ISession)CallContext.GetData(SessionKey);
            }
            set
            {
                if (IsInWebContext())
                {
                    HttpContext.Current.Items[SessionKey] = value;
                }
                else
                {
                    CallContext.SetData(SessionKey, value);
                }
            }
        }

        private static bool IsInWebContext()
        {
            return HttpContext.Current != null;
        }

        private const string TransactionKey = "CONTEXT_TRANSACTION";
        private const string SessionKey = "CONTEXT_SESSION";
        private ISessionFactory _sessionFactory;
        private Configuration _configuration;
    }

}
