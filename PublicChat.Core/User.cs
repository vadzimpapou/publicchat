﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Timers;
using System.Web.Mvc;


namespace PublicChat.Core
{
    public class User
    {
       // public int Id { get; set; }

        [Required(ErrorMessage = "Введите имя пользователя")]
        [Display(Name = "Имя пользователя")]
        [Remote("IsUserRegistered","Home")]
        public string Name { get; set; }
        public override bool Equals(object obj)
        {
            var user = obj as User;
            if (user != null)
            {
                if ((user).Name == Name)
                    return true;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return (Name != null) ? Name.GetHashCode() : 0;
        }
    }
}
