﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace PublicChat.Core
{
    public class Message
    {
        public virtual int Id { get; set; }
        [Display(Name = "Текст сообщения")]
        public virtual string Text { get; set; }
        public virtual string AuthorName { get; set; }
    }
}
