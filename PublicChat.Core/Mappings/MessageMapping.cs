﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace PublicChat.Core.Mappings
{
    public class MessageMapping:ClassMap<Message>
    {
        public MessageMapping()
        {
            Table("Messages");
            Id(m => m.Id).GeneratedBy.Native();
            Map(m => m.Text);
            Map(m => m.AuthorName);
        }
    }
}
