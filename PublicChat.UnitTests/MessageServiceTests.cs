﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using PublicChat.Functional.Service;

namespace PublicChat.UnitTests
{
    [TestFixture]
    public class MessageServiceTests
    {
        private Mock<MessageService> _sut;



        [SetUp]
        public void SetUp()
        {
            _sut = new Mock<MessageService>();
        }

        [Test]
        public void GetSomeMessage_WhenDrIsOk_ReturnsMessage()
        {

            var dr = new Mock<IDataReader>();
            dr.Setup(x => x["Text"]).Returns("My text");
            dr.Setup(x => x["Id"]).Returns("5");
            dr.Setup(x => x["AuthorName"]).Returns("Me");

            var result = _sut.Object.GetSomeMessage(dr.Object);

            Assert.IsNotNull(result);
            Assert.AreEqual(5,result.Id);
            Assert.AreEqual("My text", result.Text);
            Assert.AreEqual("Me", result.AuthorName);
        }

        [TearDown]
        public void TearDown()
        {
            _sut = null;
        }
    }
}
